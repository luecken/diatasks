package main

import (
	"fmt"
	"os"

	"gitlab.com/luecken/diatask/skiplist"
)

func main() {
	fmt.Println(os.Args)
	if len(os.Args) < 2 {
		fmt.Println("Usage: diatask <filename>")
		os.Exit(1)
	}
	fn := os.Args[1]
	//fn := "testints.txt"
	skiplist.Test(fn)
}

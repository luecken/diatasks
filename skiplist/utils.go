package skiplist

import (
	"io/ioutil"
	"log"
	"math/rand"
	"strconv"
	"strings"
)

// min of integers... (not in stdlib?!)
func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// readIntArray read a list of integers from a file
func readIntArray(fn string) []int64 {
	content, err := ioutil.ReadFile(fn)
	if err != nil {
		log.Fatal(err)
	}
	str := string(content)
	//fmt.Printf("Read from file:\n'%s'\n\n", str)
	res := make([]int64, 0)
	for _, xstr := range strings.Fields(str) {
		x, err := strconv.ParseInt(xstr, 0, 64)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, x)
	}
	return res
}

func randLevel(maxLvl int) int {
	var lvl int
	for lvl = 0; rand.Float64() < p && lvl < maxLvl; lvl++ {
	}
	return lvl
}

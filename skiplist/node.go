package skiplist

type node struct {
	x    int64
	lvl  int
	next []*node // next larger key
	prev []*node // next smaller key
}

func newNode(x int64, lvl int) *node {
	return &node{x, lvl, make([]*node, lvl+1), make([]*node, lvl+1)}
}

// Find neighboring nodes of value x on level lvl starting from node start
// if neighbors are not nil, this guarantees x in halfopen interval [prev.x, next.x)
func findNeighs(x int64, start *node, lvl int) (*node, *node) {
	var prev, next *node
	if x < start.x {
		// search left
		for prev, next = start.prev[lvl], start; prev != nil; prev, next = prev.prev[lvl], prev {
			if x >= prev.x {
				break
			}
		}
		return prev, next
	}
	// search right
	for prev, next = start, start.next[lvl]; next != nil; prev, next = next, next.next[lvl] {
		if x < next.x {
			return prev, next
		}
	}
	return prev, next
}

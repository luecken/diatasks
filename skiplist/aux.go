package skiplist

import (
	"fmt"
	"strings"
)

func (l *Skiplist) String() string {
	res := make([]string, 0)
	for n := l.findMin(); n != nil; n = n.next[0] {
		res = append(res, fmt.Sprintf("%d (%d)", n.x, n.lvl))
	}
	return "[" + strings.Join(res, ", ") + "]"
}

func (l *Skiplist) empty() bool {
	return l.head == nil
}

func (l *Skiplist) findMin() *node {
	if l.empty() {
		return nil
	}
	var min *node
	for min = l.head; min.prev[0] != nil; min = min.prev[0] {
	}
	return min
}

func findAlternativeHead(oldHead *node) *node {
	if oldHead == nil {
		return nil
	}
	for lvl := oldHead.lvl; lvl >= 0; lvl-- {
		if oldHead.next[lvl] != nil {
			return oldHead.next[lvl]
		}
		if oldHead.prev[lvl] != nil {
			return oldHead.prev[lvl]
		}
	}
	// list is empty apart of oldHead
	return nil
}

// deleteNode removes given Node from list by rewiring its neighbors
// updating the list's head
// (assumes node to be in list unchecked)
func (l *Skiplist) deleteNode(n *node) {
	if n == l.head {
		l.head = findAlternativeHead(n)
	}
	// Rewire neighbors
	for lvl := n.lvl; lvl >= 0; lvl-- {
		prev, next := n.prev[lvl], n.next[lvl]
		if prev != nil {
			prev.next[lvl] = next
		}
		if next != nil {
			next.prev[lvl] = prev
		}
	}
}

// searchNode returns the first node found holding value x
func (l *Skiplist) searchNode(x int64) *node {
	if l.empty() {
		return nil
	}
	current := l.head
	for lvl := l.head.lvl; lvl >= 0; lvl-- {
		prev, _ := findNeighs(x, current, lvl)
		if prev != nil && prev.x == x {
			return prev
		}
	}
	return nil
}

// InitWithArray ..
func (l *Skiplist) InitWithArray(arr []int64) {
	for _, x := range arr {
		l.Insert(x)
	}
}

// New creates new empty Skiplist
func New() *Skiplist {
	return &Skiplist{nil}
}

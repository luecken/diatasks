package skiplist

import "fmt"

// Test tests stuff :) (called by main)
func Test(fn string) {
	fmt.Println("Skiplist test ....")
	arr := readIntArray(fn)
	fmt.Printf("In:  %v\n", arr)
	l := New()
	l.Init(fn)
	fmt.Printf("Out: %v\n", l.String())

	// var x int64 = 4
	// present, ix := l.Search(x)
	// fmt.Printf("found %d: %v\nat ix: %v\n", x, present, ix)

	// fmt.Printf("Delete: %v\n", x)
	// l.Delete(x)
	// fmt.Printf("Out: %v\n", l.String())

	// fmt.Printf("Insert: %v\n", x)
	// l.Insert(x)
	// fmt.Printf("Out: %v\n", l.String())

	// x = -1000
	// fmt.Printf("Insert: %v\n", x)
	// l.Insert(x)
	// fmt.Printf("Out: %v\n", l.String())

	// x = 1000
	// fmt.Printf("Insert: %v\n", x)
	// l.Insert(x)
	// fmt.Printf("Out: %v\n", l.String())

}

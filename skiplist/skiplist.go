package skiplist

// Nr of levels
const maxdepth int = 16

// proportion of level occupancy
const p float64 = 0.5

// Skiplist ...
type Skiplist struct {
	head *node
}

// Init inits a new list from file
func (l *Skiplist) Init(fn string) {
	l.InitWithArray(readIntArray(fn))
}

// Search ..
func (l *Skiplist) Search(x int64) (bool, int64) {
	nx := l.searchNode(x)
	if nx == nil {
		return false, 0
	}
	// Find index (not efficient)
	var ix int64 = 0
	for n := l.findMin(); n != nx; n = n.next[0] {
		ix++
	}
	return true, ix
}

// Insert inserts new Node with value x into the list
func (l *Skiplist) Insert(x int64) {
	if l.empty() {
		// Insert new node as head on level 0
		l.head = newNode(x, 0)
		return
	}
	// assign random level for new node
	nlvl := randLevel(maxdepth)
	nx := newNode(x, nlvl)

	// Descent through levels from current header level
	start := l.head
	for lvl := min(l.head.lvl, nlvl); lvl >= 0; lvl-- {
		// Find insertion position on current level
		nx.prev[lvl], nx.next[lvl] = findNeighs(x, start, lvl)
		// choose new start on next lower level
		if nx.prev[lvl] == nil {
			start = nx.next[lvl]
		} else {
			start = nx.prev[lvl]
		}
	}

	// Perform insertions
	for lvl := min(l.head.lvl, nlvl); lvl >= 0; lvl-- {
		if nx.prev[lvl] != nil {
			nx.prev[lvl].next[lvl] = nx
		}
		if nx.next[lvl] != nil {
			nx.next[lvl].prev[lvl] = nx
		}
	}

	if nx.lvl > l.head.lvl {
		// Make nx new head if it has maximal lvl in list
		l.head = nx
	}
}

// Delete finds all occurences of x and erases them
func (l *Skiplist) Delete(x int64) {
	// start search on highest level
	for {
		nx := l.searchNode(x)
		if nx != nil {
			l.deleteNode(nx)
		} else {
			break
		}
	}
}
